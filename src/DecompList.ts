import { Decomp } from './Decomp';
import { Vector } from './Vector';
import type { ReasembList } from './ReasembList';

export class DecompList extends Vector<Decomp> {

    add(word:string, mem:boolean, reasmb:ReasembList) {
        this._items.push(new Decomp(word, mem, reasmb));
    }
    get(i:number) {
        return this._items[i];
    }
    print (indent:number) {
        for (let i=0; i<this.size(); i++) {
            this._items[i].print(indent);
        }
    }
}



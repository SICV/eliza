export class Vector<T> {
    protected _items:T[];

    constructor () {
        this._items = [];
    }

    elementAt (i:number) {
        return this._items[i];
    }

    size ():number {
        return this._items.length;
    }

    // find (elt:T):T|null {
    //     for (let i=0; i<this._items.length; i++) {
    //         if (this._items[i] == elt) {
    //             this._items[i];
    //         }
    //     }
    //     return null;
    // }

}
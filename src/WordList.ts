import { Vector } from './Vector';

export class WordList extends Vector<string> {
    add (word:string) {
        this._items.push(word);
    }
    print (indent:number) {
        let ret = '';
        for (let i=0; i<this.size(); i++) {
            ret += this._items[i] + "  ";
        }
        console.log(ret);
    }

    find (s:string):boolean {
        for (let i=0; i<this.size(); i++) {
            if (this._items[i] == s) {
                return true;
            }
        }
        return false;
    }
}

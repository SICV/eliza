/**
 *  Eliza memory class
 */

export class Mem {
    memMax = 20;
    memory = [];
    memTop = 0;

    save (str:string) {
        if (this.memTop < this.memMax) {
            this.memory[this.memTop++] = str;
        }
    }

    get():string|null {
        if (this.memTop == 0) return null;
        let m = this.memory[0];
        for (let i=0; i<this.memTop-1; i++) {
            this.memory[i] = this.memory[i+1];
        }
        this.memTop--;
        return m;
    }
}

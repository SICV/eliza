import type { Decomp } from "./Decomp";
import type { DecompList } from "./DecompList";

export class Key {
    protected _key:string|null;
    protected _rank:number;
    protected _decomp:DecompList|null;

    constructor (key:string = null, rank:number = 0, decomp:DecompList = null) {
        this._key = key;
        this._rank = rank;
        this._decomp = decomp;
    }
    // constructor () {
    //     this._key = null;
    //     this._rank = 0;
    //     this._decomp = null;
    // }

    copy (k:Key) {
        this._key = k.key();
        this._rank = k.rank();
        this._decomp = k.decomp();
    }

    
    /**
     *  Print the key and all under it.
     */
    print(indent:number) {
        let n = '';
        for (let i = 0; i < indent; i++) n += ' ';
        console.log(n + "key: " + this._key + " " + this._rank);
        this._decomp.print(indent+2);
    }

    /**
     *  Print the key and rank only, not the rest.
     */
    printKey(indent:number) {
        let n = '';
        for (let i = 0; i < indent; i++) n += ' ';
        console.log(n + "key: " + this._key + " " + this._rank);
    }

    key () {
        return this._key;
    }
    rank () {
        return this._rank;
    }
    decomp () {
        return this._decomp;
    }
}
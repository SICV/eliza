import { compute_rest_props, compute_slots } from "svelte/internal";

export class EString {
    constructor () {

    }
    private static num = "0123456789";

    /* Look for a match between the string and the pattern.
    Return count of matching characters before * or #
    Return -1 if strings do not match
    */
    public static amatch (str:string, pat:string):number {
        let i = 0;
        while (i<str.length && i < pat.length) {
            let p = pat.charAt(i);
            if (p == '*' || p == '#') return i;
            if (str.charAt(i) != p) return -1;
            // still equal
            i++;
        }
        return i;
    }

    /**
     *  Search in successive positions of the string,
     *  looking for a match to the pattern.
     *  Return the string position in str of the match,
     *  or -1 for no match.
     */
     public static findPat(str:string, pat:string):number {
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (this.amatch(str.substring(i), pat) >= 0)
                return count;
            count++;
        }
        return -1;
    }
    /**
     *  Look for a number in the string.
     *  Return the number of digits at the beginning.
     */
     public static findNum(str:string):number {
        let count = 0;
        for (let i = 0; i < str.length; i++) {
            if (this.num.indexOf(str.charAt(i)) == -1)
                return count;
            count++;
        }
        return count;
    }

    /**
     *  Match the string against a pattern and fills in
     *  matches array with the pieces that matched * and #
     */
    protected static matchA(str:string, pat:string, matches:string[]):boolean {
        let i = 0;      //  move through str
        let j = 0;      //  move through matches
        let pos = 0;    //  move through pat
        // console.log(`matchA: str: ${str}, pat: ${pat}, matches: ${matches.length}`)
        while (pos < pat.length && j < matches.length) {
            const p = pat.charAt(pos);
            if (p == '*') {
                let n;
                if (pos+1 == pat.length) {
                    //  * is the last thing in pat
                    //  n is remaining string length
                    n = str.length - i;
                } else {
                    //  * is not last in pat
                    //  find using remaining pat
                    n = this.findPat(str.substring(i), pat.substring(pos+1));
                }
                if (n < 0) return false;
                matches[j++] = str.substring(i, i+n);
                i += n; pos++;
            } else if (p == '#') {
                let n = this.findNum(str.substring(i));
                matches[j++] = str.substring(i, i+n);
                i += n; pos++;
            } else {
                let n = this.amatch(str.substring(i), pat.substring(pos));
                if (n <= 0) return false;
                i += n; pos += n;
            }
        }
        if (i >= str.length && pos >= pat.length) return true;
        return false;
    }

    static match(str:string, pat:string, matches:string[]):boolean {
        return this.matchA(str, pat, matches);
    }

    /*
     *  Translates corresponding characters in src to dest.
     *  Src and dest must have the same length.
     */
    public static translate(str:string, src:string, dest:string):string {
        if (src.length != dest.length) {
            // impossible error
            throw new Error("translate called on different length strings");
        }
        for (let i = 0; i < src.length; i++) {
            str = str.replace(src.charAt(i), dest.charAt(i));
        }
        return str;
    }

    /**
     *  Compresses its input by:
     *    dropping space before space, comma, and period;
     *    adding space before question, if char before is not a space; and
     *    copying all others
     */
    public static compress(s:string):string {
        let dest = "";
        if (s.length == 0) return s;
        let c = s.charAt(0);
        for (let i = 1; i < s.length; i++) {
            if (c == ' ' &&
                 ((s.charAt(i) == ' ') ||
                 (s.charAt(i) == ',') ||
                 (s.charAt(i) == '.'))) {
                    // nothing
            } else if (c != ' ' && s.charAt(i) == '?') {
                dest += c + " ";
            } else {
                dest += c;
            }
            c = s.charAt(i);
        }
        dest += c;
        return dest;
    }

    /**
     *  Trim off leading space
     */
    public static trim(s:string):string {
        for (let i = 0; i < s.length; i++) {
            if (s.charAt(i) != ' ') return s.substring(i);
        }
        return "";
    }

    /**
     *  Pad by ensuring there are spaces before and after the sentence.
     */
    public static pad(s:string):string {
        if (s.length == 0) return " ";
        let first = s.charAt(0);
        let last = s.charAt(s.length-1);
        if (first == ' ' && last == ' ') return s;
        if (first == ' ' && last != ' ') return s + " ";
        if (first != ' ' && last == ' ') return " " + s;
        if (first != ' ' && last != ' ') return " " + s + " ";
        // impossible
        return s;
    }

    /**
     *  Count number of occurrances of c in str
     */
    public static count(s:string, c:string):number {
        let count = 0;
        for (let i = 0; i < s.length; i++)
            if (s.charAt(i) == c) count++;
        return count;
    }

}
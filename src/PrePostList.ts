import { PrePost } from './PrePost';
import { Vector } from './Vector';
import { EString } from './EString';

export class PrePostList extends Vector<PrePost> {
    add (src:string, dest:string) {
        this._items.push(new PrePost(src, dest));
    }

    print (indent:number) {
        for (let i=0; i<this.size(); i++) {
            this._items[i].print(indent);
        }
    }

    /**
     *  Translate a string.
     *  If str matches a src string on the list,
     *  return he corresponding dest.
     *  If no match, return the input.
     */
     xlate(str:string):string {
        for (let i = 0; i < this.size(); i++) {
            let p = this.elementAt(i);
            if (str == p.src()) {
                return p.dest();
            }
        }
        return str;
    }

    /**
     *  Translate a string s.
     *  (1) Trim spaces off.
     *  (2) Break s into words.
     *  (3) For each word, substitute matching src word with dest.
     */
    translate(s:string):string {
        let lines = ['','']; // new String[2];
        let work = EString.trim(s);
        s = "";
        while (EString.match(work, "* *", lines)) {
            s += this.xlate(lines[0]) + " ";
            work = EString.trim(lines[1]);
        }
        s += this.xlate(work);
        return s;
    }

}

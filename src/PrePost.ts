
export class PrePost {
    protected _src:string;
    protected _dest:string;
    constructor (src:string, dest:string) {
        this._src = src;
        this._dest = dest;
    }
    src ():string { return this._src; }
    dest ():string { return this._dest; }
    print (indent:number) {
        let n = '';
        for (let i=0; i<indent; i++) n += ' ';
        console.log(n + "pre-post: " + this._src + " " + this._dest);
    }

}
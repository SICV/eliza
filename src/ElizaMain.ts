/**
 *  Eliza main class.
 *  Stores the processed script.
 *  Does the input transformations.
 */

import { EString } from './EString';
import { KeyList } from './KeyList';
import { SynList } from './SynList';
import { PrePostList } from './PrePostList';
import { WordList } from './WordList';
import { KeyStack } from './KeyStack';
import { Key } from './Key';
import { Mem } from './Mem';
import { DecompList } from './DecompList';
import { ReasembList } from './ReasembList';
import type { Decomp } from './Decomp';

export class ElizaMain {
 
    // echoInput = false;
 
    constructor(
        protected echoInput:boolean = false,
        protected printData = false
    ) {}

    printKeys = true;
    printSyns = true;
    printPrePost = true;
    printInitialFinal = true;

    /** The key list */
    keys = new KeyList();
    /** The syn list */
    syns = new SynList();
    /** The pre list */
    pre = new PrePostList();
    /** The post list */
    post = new PrePostList();
    /** Initial string */
    initial = "Hello.";
    /** Final string */
    finl = "Goodbye.";
    /** Quit list */
    quit = new WordList();

    /** Key stack */
    keyStack = new KeyStack();

    /** Memory */
    mem = new Mem();

    lastDecomp:DecompList | null = null;
    lastReasemb:ReasembList | null = null;
    _finished = false;

    static success = 0;
    static failure = 1;
    static gotoRule = 2;

    public finished():boolean {
        return this._finished;
    }

    /**
     *  Process a line of script input.
     */
    public collect(s:string) {
        let lines = ['','','','']; // new String[4];

        if (EString.match(s, "*reasmb: *", lines)) {
            if (this.lastReasemb == null) {
                console.log("Error: no last reasemb");
                return;
            }
            this.lastReasemb.add(lines[1]);
        }
        else if (EString.match(s, "*decomp: *", lines)) {
            if (this.lastDecomp == null) {
                console.log("Error: no last decomp");
                return;
            }
            this.lastReasemb = new ReasembList();
            let temp = lines[1]; // new String(lines[1]);
            if (EString.match(temp, "$ *", lines)) {
                this.lastDecomp.add(lines[0], true, this.lastReasemb);
            } else {
                this.lastDecomp.add(temp, false, this.lastReasemb);
            }
        }
        else if (EString.match(s, "*key: * #*", lines)) {
            this.lastDecomp = new DecompList();
            this.lastReasemb = null;
            let n = 0;
            if (lines[2].length != 0) {
                n = parseInt(lines[2]);
                if (n == NaN) {
                    console.log("Number is wrong in key: " + lines[2]);                    
                }
            }
            this.keys.add(lines[1], n, this.lastDecomp);
        }
        else if (EString.match(s, "*key: *", lines)) {
            this.lastDecomp = new DecompList();
            this.lastReasemb = null;
            this.keys.add(lines[1], 0, this.lastDecomp);
        }
        else if (EString.match(s, "*synon: * *", lines)) {
            let words = new WordList();
            words.add(lines[1]);
            s = lines[2];
            while (EString.match(s, "* *", lines)) {
                words.add(lines[0]);
                s = lines[1];
            }
            words.add(s);
            this.syns.add(words);
        }
        else if (EString.match(s, "*pre: * *", lines)) {
            this.pre.add(lines[1], lines[2]);
        }
        else if (EString.match(s, "*post: * *", lines)) {
            this.post.add(lines[1], lines[2]);
        }
        else if (EString.match(s, "*initial: *", lines)) {
            this.initial = lines[1];
        }
        else if (EString.match(s, "*final: *", lines)) {
            this.finl = lines[1];
        }
        else if (EString.match(s, "*quit: *", lines)) {
            this.quit.add(" " + lines[1]+ " ");
        }
        else {
            console.log("Unrecognized input: " + s);
        }
    }

    /**
     *  Print the stored script.
     */
    public print() {
        if (this.printKeys) this.keys.print(0);
        if (this.printSyns) this.syns.print(0);
        if (this.printPrePost) {
            this.pre.print(0);
            this.post.print(0);
        }
        if (this.printInitialFinal) {
            console.log("initial: " + this.initial);
            console.log("final:   " + this.finl);
            this.quit.print(0);
        }
    }

    /**
     *  Process a line of input.
     *  Main function to call to write a message to eliza, returns response
     */
    processInput(s:string):string {
        let reply:string = '';
        //  Do some input transformations first.
        s = EString.translate(s, "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
                                 "abcdefghijklmnopqrstuvwxyz");
        s = EString.translate(s, "@#$%^&*()_-+=~`{[}]|:;<>\\\"",
                                 "                          "  );
        s = EString.translate(s, ",?!", "...");
        //  Compress out multiple speace.
        s = EString.compress(s);
        let lines = ['','']; // new String[2];
        //  Break apart sentences, and do each separately.
        while (EString.match(s, "*.*", lines)) {
            reply = this.sentence(lines[0]);
            if (reply != null) return reply;
            s = EString.trim(lines[1]);
        }
        if (s.length != 0) {
            reply = this.sentence(s);
            if (reply != null) return reply;
        }
        // console.log("processInput: Nothing matched, so try memory");
        //  Nothing matched, so try memory.
        let m = this.mem.get();
        if (m != null) return m;

        //  No memory, reply with xnone.
        let key = this.keys.getKey("xnone");
        if (key != null) {
            reply = this.decompose(key, s, null);
            if (reply != null) return reply;
        }
        //  No xnone, just say anything.
        return "I am at a loss for words.";
    }

    /**
     *  Process a sentence.
     *  (1) Make pre transformations.
     *  (2) Check for quit word.
     *  (3) Scan sentence for keys, build key stack.
     *  (4) Try decompositions for each key.
     */
    protected sentence(s:string):string|null {
        s = this.pre.translate(s);
        s = EString.pad(s);
        if (this.quit.find(s)) {
            this._finished = true;
            return this.finl;
        }
        this.keys.buildKeyStack(this.keyStack, s);
        for (let i = 0; i < this.keyStack.keyTop(); i++) {
            let gotoKey = new Key();
            let reply = this.decompose(this.keyStack.key(i), s, gotoKey);
            if (reply != null) return reply;
            //  If decomposition returned gotoKey, try it
            while (gotoKey.key() != null) {
                reply = this.decompose(gotoKey, s, gotoKey);
                if (reply != null) return reply;
            }
        }
        return null;
    }

    /**
     *  Decompose a string according to the given key.
     *  Try each decomposition rule in order.
     *  If it matches, assemble a reply and return it.
     *  If assembly fails, try another decomposition rule.
     *  If assembly is a goto rule, return null and give the key.
     *  If assembly succeeds, return the reply;
     */
    decompose(key:Key, s:string, gotoKey:Key|null):string {
        let reply = ['','','','','','','','','','']; // new String[10];
        for (let i = 0; i < key.decomp().size(); i++) {
            let d = key.decomp().elementAt(i);
            let pat = d.pattern();
            if (this.syns.matchDecomp(s, pat, reply)) {
                let rep = this.assemble(d, reply, gotoKey);
                if (rep != null) return rep;
                if (gotoKey.key() != null) return null;
            }
        }
        return null;
    }

    /**
     *  Assembly a reply from a decomp rule and the input.
     *  If the reassembly rule is goto, return null and give
     *    the gotoKey to use.
     *  Otherwise return the response.
     */
    assemble(d:Decomp, reply:string[], gotoKey:Key):string|null {
        let lines = ['','','']; // new String[3];
        d.stepRule();
        let rule = d.nextRule();
        if (EString.match(rule, "goto *", lines)) {
            //  goto rule -- set gotoKey and return false.
            gotoKey.copy(this.keys.getKey(lines[0]));
            if (gotoKey.key() != null) return null;
            console.log("Goto rule did not match key: " + lines[0]);
            return null;
        }
        let work = "";
        while (EString.match(rule, "* (#)*", lines)) {
            //  reassembly rule with number substitution
            rule = lines[2];        // there might be more
            let n = 0;
            n = parseInt(lines[1]);
            if (n == NaN) {
                console.log("Number is wrong in reassembly rule " + lines[1]);
            } else {
                n = n - 1;
            }
            if (n < 0 || n >= reply.length) {
                console.log("Substitution number is bad " + lines[1]);
                return null;
            }
            reply[n] = this.post.translate(reply[n]);
            work += lines[0] + " " + reply[n];
        }
        work += rule;
        if (d.mem()) {
            this.mem.save(work);
            return null;
        }
        return work;
    }

    /*
    TextArea textarea;
    TextField textfield;

    public void response(String str) {
        textarea.appendText(str);
        textarea.appendText("\n");
    }
    */

    async readScript(script:string) {
        const resp = await fetch(script);
        const text = await resp.text();
        const lines = text.split(/\r?\n/);
        // console.log("lines", lines);
        for (let i=0; i<lines.length; i++) {
            if (this.echoInput) console.log(lines[i]);
            if (lines[i]) {
                this.collect(lines[i]);
            }
        }
        if (this.printData) this.print();
    }

 }

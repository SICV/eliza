import type { ReasembList } from "./ReasembList";

export class Decomp {
    protected _pattern:string;
    protected _mem:boolean;
    reasemb:ReasembList;
    currReasmb:number;

    constructor (pattern:string, mem:boolean, reasemb:ReasembList) {
        this._pattern = pattern;
        this._mem = mem;
        this.reasemb = reasemb;
        this.currReasmb = 100;
    }

    /**
     *  Print out the decomp rule.
     */
    print(indent:number) {
        let m = this._mem ? "true" : "false";
        let n = '';
        for (let i = 0; i < indent; i++) n += " ";
        console.log(n + "decomp: " + this._pattern + " " + m);
        this.reasemb.print(indent + 2);
    }

    /**
     *  Get the pattern.
     */
    pattern():string {
        return this._pattern;
    }

    /**
     *  Get the mem flag.
     */
    mem():boolean {
        return this._mem;
    }

    /**
     *  Get the next reassembly rule.
     */
    nextRule():string {
        if (this.reasemb.size() == 0) {
            console.log("No reassembly rule.");
            return null;
        }
        return this.reasemb.elementAt(this.currReasmb);
    }

    /**
     *  Step to the next reassembly rule.
     *  If mem is true, pick a random rule.
     */
    stepRule() {
        let size = this.reasemb.size();
        if (this._mem) {
            this.currReasmb = Math.floor(Math.random() * size);
        }
        //  Increment and make sure it is within range.
        this.currReasmb++;
        if (this.currReasmb >= size) this.currReasmb = 0;
    }


}
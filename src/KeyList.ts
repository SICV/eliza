import { Key } from './Key';
import { Vector } from './Vector'
import type { DecompList } from './DecompList';
import type { KeyStack } from './KeyStack';
import { EString } from './EString';

export class KeyList extends Vector<Key> {
    add(key:string, rank:number, decomp:DecompList) {
        this._items.push(new Key(key, rank, decomp));
    }

    print (indent:number) {
        for (let i=0; i<this.size(); i++) {
            this._items[i].print(indent);
        }
    }

    getKey(s:string):Key {
        for (let i=0; i<this.size(); i++) {
            let key = this.elementAt(i);
            if (s == key.key()) return key;
        }
        return null;
    }

    buildKeyStack(stack:KeyStack, s:string) {
        stack.reset();
        s = EString.trim(s);
        let lines = ['','']; // new String[2]
        let k:Key;
        while (EString.match(s, '* *', lines)) {
            k = this.getKey(lines[0]);
            if (k !== null) stack.pushKey(k);
            s = lines[1];
        }
        k = this.getKey(s);
        if (k !== null) stack.pushKey(k);
        // stack.print();
    }
}

import { EString } from './EString';
import type { WordList } from './WordList';
import { Vector } from './Vector';


/**
 *  Eliza synonym list.
 *  Collection of all the synonym elements.
 */
export class SynList extends Vector<WordList> {
    add(words:WordList) {
        this._items.push(words);
    }

    print (indent:number) {
        let n = '';
        for (let j=0; j<indent; j++) n += ' ';
        for (let i=0; i<this.size(); i++) {
            console.log(n + "synon: ");
            this._items[i].print(indent);
        }
    }

    find(s:string):WordList | null {
        for (let i=0; i<this.size(); i++) {
            if (this._items[i].find(s)) return this._items[i];
        }
        return null;
    }

    matchDecomp (str:string, pat:string, lines: string[]) {
        if (! EString.match(pat, "*@* *", lines)) {
            return EString.match(str, pat, lines);
        }
        let first = lines[0];
        let synWord = lines[1];
        let theRest = " " + lines[2];
        // Look up the synonym
        let syn = this.find(synWord);
        if (syn == null) {
            console.log("Could not find syn list for " + synWord);
            return false;
        }
        // Try each synonym individually
        for (let i=0; i<syn.size(); i++) {
            // Make a modified pattern
            pat = first + syn.elementAt(i) + theRest;
            if (EString.match(str, pat, lines)) {
                let n = EString.count(first, '*');
                for (let j = lines.length-2; j>=n; j--) {
                    // Make room for the synonym in the match list.
                    lines[j+1] = lines[j];
                }
                // The synonym goes in the match list.
                lines[n] = syn.elementAt(i);
                return true;
            }
        }
        return false;
    }
}

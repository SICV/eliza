/**
 *  A stack of keys.
 *  The keys are kept in rank order.
 */

import type { Key } from './Key';

export class KeyStack {
    protected stackSize = 20;
    protected keyStack:Key[] = []; 
    protected _keyTop = 0;

    print () {
        console.log("Key stack " + this._keyTop);
        for (let i=0; i<this._keyTop; i++) {
            this.keyStack[i].printKey(0);
        }
    }

    keyTop():number { return this._keyTop; }

    reset () {
        this._keyTop = 0;
    }
    
    /**
     *  Get a key from the stack.
     */
    key(n:number):Key {
        if (n < 0 || n >= this._keyTop) return null;
        return this.keyStack[n];
    }
    /**
     *  Push a key in the stack.
     *  Keep the highest rank keys at the bottom.
     */
    pushKey (key:Key) {
        if (key == null) {
            console.log("push null key");
            return;
        }
        let i;
        for (i=this._keyTop; i>0; i--) {
            if (key.rank() > this.keyStack[i-1].rank()) {
                this.keyStack[i] = this.keyStack[i-1];
            } else {
                break;
            }
        }
        this.keyStack[i] = key;
        this._keyTop++;
    }
}
